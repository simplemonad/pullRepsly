function pullRepslyVisit() {
  
  var options = {};
  options.headers = {"Authorization": "Basic " + Utilities.base64Encode("USERID" + ":" + "PASSWORD")}; // replace by your data from Repsly settings
  
  var ss = SpreadsheetApp.openById('SHEETID'); // replace by ID of Google Sheet where you wanna see your data
  var sheets = ss.getSheets();
  var sheet = ss.getActiveSheet();
  
  var lrow = ss.getLastRow();
  var lcol = ss.getLastColumn();
  
   
  var range = sheet.getRange(lrow,lcol); 
  var stamp = range.getValue();
  
  if (stamp === "lastTimeStamp") {stamp = "0"} // ugly hack, will sort it soon

  var url="https://api.repsly.com/v3/export/visits/"+stamp; // Paste your JSON URL here
  
  var response = UrlFetchApp.fetch(url, options); // get feed
  var dataAll = JSON.parse(response.getContentText()); //
  var dataSet = dataAll;  
  
  var rows = [],
      data;  

  for (i = 0; i < dataSet.Visits.length; i++) {
    data = dataSet.Visits[i];

    datum = new Date(parseInt(data.Date.slice(6,19)));
    datumStart = new Date(parseInt(data.DateAndTimeStart.slice(6,19)));
    datumEnd = new Date(parseInt(data.DateAndTimeEnd.slice(6,19)));
    
    lastTimeStamp = dataSet.MetaCollectionResult.LastTimeStamp;
    
    rows.push([
      data.VisitID,
      data.TimeStamp,
      datum,
      data.RepresentativeCode,
      data.RepresentativeName,
      data.ExplicitCheckIn,
      datumStart,
      datumEnd,
      data.ClientCode,
      data.ClientName,
      data.StreetAddress,
      data.ZIP,
      data.ZIPExt,
      data.City,
      data.State,
      data.Country,
      data.Territory,
      data.LatitudeStart,
      data.LongitudeStart,
      data.LatitudeEnd,
      data.LongitudeEnd,
      data.PrecisionStart,
      data.PrecisionEnd,
      data.VisitStatusBySchedule,
      data.VisitEnded,
      lastTimeStamp
    ]); //your JSON entities here
  }
  
  // [row to start on], [column to start on], [number of rows], [number of entities]
  dataRange = sheet.getRange(lrow+1, 1, rows.length, 26);
  dataRange.setValues(rows);

}